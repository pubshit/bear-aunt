let customer = {
  lastName: "",
  firstName: "",
  email: "",
  phone: "",
  province: {},
  district: {},
  commune: {},
  address: "",
  note: "",
};

function cartInit() {
  mapCart();
  request.totalItem();
}

function mapCart() {
  let getData = request.getItemLocalStorage(keyCart);
  const rowBody = document.querySelector(".row__body");
  if (rowBody)
    rowBody.innerHTML = !getData
      ? ""
      : getData
          ?.map((item) => {
            return `
            <tr class="body__row flex">
                <td class="body__cell">
                        <img src="${item.url}" alt="" class="img__body">
                </td>
                <td class="body__cell item__name">${item.name}</td>
                <td class="body__cell">${item.price} $ per one</td>
                <td class="body__cell">
                    <div id="subTract" class="btn__action" onclick="subtractItemNumber('${item.id}')">-</div>           
                    <div class="item__number flex">${item.soLuong}</div>
                    <div id="plus" class="btn__action" 
                    onclick="plusItemNumber('${item.id}',
                         '${item.name}',
                          '${item.price}',
                          '${item.description}',
                         '${item.url}',)"
                    >+</div>
                </td>
                <td class="body__cell">${item.total} $</td>
                <td class="body__cell">
                    <button 
                    type="button" 
                    class="btn btn-outline-danger btn__delete"
                    onclick="openConfirmDialog('${item.id}')"
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                        </svg>
                    </button>
                </td>
            </tr>
        `;
          })
          .join("");
  request.totalItem();
}

function subtractItemNumber(itemId) {
  let listCartItem = request.getItemLocalStorage(keyCart);
  let listData = request.getItemLocalStorage(keyLocalStorageListSP);
  let getCartItem = listCartItem.find((item) => item.id === itemId);

  if (getCartItem.soLuong > 1) {
    getCartItem.soLuong--;
    listData.find((item) => (item.id === itemId ? item.quantity++ : ""));
    request.setItemLocalStorage(keyLocalStorageListSP, listData);
    request.setItemLocalStorage(keyCart, listCartItem);
    request.totalItem();
  } else alertMessage("Can not reduce more the product", "warning");
  mapCart();
}

//add more product
function plusItemNumber(id, name, price, description, url) {
  addSP(id, name, price, description, url);
  mapCart();
}

// confirm Dialog actions
function openConfirmDialog(itemID) {
  let confirmDialogElement = document.querySelector(".confirm__dialog");
  confirmDialogElement.style.display = "flex";
  request.setItemLocalStorage(keyItemRemove, itemID);
}

function closeConfirmDialog() {
  let confirmDialogElement = document.querySelector(".confirm__dialog");
  confirmDialogElement.style.display = "none";
  request.removeItemLocalStorage(keyItemRemove);
}

//remove product from cart
const deleteItem = () => {
  let removeItemID = request.getItemLocalStorage(keyItemRemove);
  console.log(removeItemID);
  let cart = request.getItemLocalStorage(keyCart);
  let danhSachSP = request.getItemLocalStorage(keyLocalStorageListSP);
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].id === removeItemID) {
      danhSachSP.find((item) => {
        if (item.id === removeItemID) {
          item.quantity += cart[i].soLuong;
          request.setItemLocalStorage(keyLocalStorageListSP, danhSachSP);
        }
      });
      cart.splice(i, 1);
      request.setItemLocalStorage(keyCart, cart);
      closeConfirmDialog();
      request.totalItem();
      mapCart();
    }
  }
};

//  dialog
const openSubmitDialog = async () => {
  const dialog = document.querySelector(".dialog__wrapper");
  refreshDialog();
  dialog.style.display = "flex";

  await mapProvinceData();
  validator({
    form: "#form",
    errorSelector: ".form__message",
    rules: [
      validator.isRequired("#input-first-name"),
      validator.isRequired("#input-last-name"),
      validator.isEmail("#input-email"),
      validator.isRequired("#input-phone"),
      validator.isRequiredSelect("#province-selection"),
      validator.isRequiredSelect("#district-selection"),
      validator.isRequiredSelect("#commune-selection"),
      validator.isRequired("#input-address"),
      validator.isRequired("#input-note"),
    ],
  });
};

const closeSubmitDialog = () => {
  document.querySelector(".dialog__wrapper").style.display = "none";
};

//Map select options
async function mapProvinceData() {
  let provinces = await request.getProvincesApi();
  provinces.map((province) => {
    document.querySelector("#province-selection").innerHTML += `
                <option class="select-option" value="${province.code}">
                    ${province.name}
                </option>
            `;
  });
}

async function handleProvinceOnchange() {
  let provinceCode = document.querySelector("#province-selection").value;
  await getDistrictsByProvinceID(provinceCode);
}

const getDistrictsByProvinceID = async (provinceCode) => {
  let districts = await request.getDistrictsApi();
  document.querySelector("#district-selection").innerHTML = `
            <option selected>Districts...</option>
        `;
  document.querySelector("#commune-selection").innerHTML = `
            <option selected >Communes...</option>
        `;
  districts
    .filter((districts) => {
      if (districts.province_code.toString() === provinceCode) {
        return districts;
      }
    })
    .map((district) => {
      document.querySelector("#district-selection").innerHTML += `
                <option class="select-option" value="${district.code}">
                    ${district.name}
                </option>
            `;
    });
};

async function handleDistrictOnchange() {
  let districtCode = document.querySelector("#district-selection").value;
  await getWardsByDistrictsID(districtCode);
}

const getWardsByDistrictsID = async (districtCode) => {
  let communes = await request.getCommunesApi();
  document.querySelector("#commune-selection").innerHTML = `
            <option selected value="null">Communes...</option>
        `;
  communes
    .filter((communes) => {
      if (communes.district_code.toString() === districtCode) return communes;
    })
    .map((commune) => {
      document.querySelector("#commune-selection").innerHTML += `
                <option class="select-option" value="${commune.code}">
                    ${commune.name}
                </option>
            `;
    });
};

function uuid() {
  let temp_url = URL.createObjectURL(new Blob());
  let uuid = temp_url.toString();
  URL.revokeObjectURL(temp_url);
  return uuid.substr(uuid.lastIndexOf("/") + 1);
}

async function getInputData() {
  let provinces = await request.getProvincesApi();
  let districts = await request.getDistrictsApi();
  let communes = await request.getCommunesApi();
  const dialog = document.querySelector(".dialog");

  provinces.find((province) => {
    if (
      province.code.toString() ===
      dialog.querySelector("#province-selection").value
    ) {
      customer.province = province;
    }
  });

  districts.find((district) => {
    if (
      district.code.toString() ===
      dialog.querySelector("#district-selection").value
    ) {
      customer.district = district;
    }
  });

  communes.find((commune) => {
    if (
      commune.code.toString() ===
      dialog.querySelector("#commune-selection").value
    ) {
      customer.commune = commune;
    }
  });

  customer.firstName = dialog.querySelector("#input-first-name").value;
  customer.lastName = dialog.querySelector("#input-last-name").value;
  customer.phone = dialog.querySelector("#input-phone").value;
  customer.email = dialog.querySelector("#input-email").value;
  customer.address = dialog.querySelector("#input-address").value;
  customer.note = dialog.querySelector("#input-note").value;
}

async function handleSubmit() {
  await getInputData();
  let cart = request.getItemLocalStorage(keyCart);
  if (checkSubmit(customer)) {
    let today = new Date().toLocaleDateString();
    let order = {
      id: uuid(),
      customer: customer,
      orderDetail: cart,
      createByDate: today,
      totalItem: cart.length,
      totalPrice: request.bill(cart),
      totalQuantity: cart.totalCartProducts(),
    };
    await request.postOrderApi(order);
    cart = [];
    request.setItemLocalStorage(keyCart, cart);
    closeSubmitDialog();
    mapCart();
    alertMessage("Your order has been ordered successfully", "success");
  } else {
    alertMessage("Please fill all the information", "error");
  }
}

cartInit();
