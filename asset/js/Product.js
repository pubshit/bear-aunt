// data
const listData = [
  {
    id: "xzfeqw",
    name: "Dazzel",
    price: 2.4,
    quantity: 46,
    description: "No added sugar",
    url: "../asset/images/dazzle.jpg",
  },
  {
    id: "rcvxwe",
    name: "Bourn Ville",
    price: 3.3,
    quantity: 35,
    description: "Rich cocoa 44% Dark",
    url: "../asset/images/BournVille.jpg",
  },
  {
    id: "xcseaw",
    name: "Caramel",
    price: 8.7,
    quantity: 29,
    description: "Four super taste",
    url: "../asset/images/caramelChocolate.jpg",
  },
  {
    id: "vcrtqw",
    name: "Carchy",
    price: 11,
    quantity: 10,
    description: "Rich cocoa mix of nuts",
    url: "../asset/images/carchy.jpg",
  },
  {
    id: "oiuvdtr",
    name: "Coccoá",
    price: 4.5,
    quantity: 60,
    description: "Frutas Silvestres taste 100g",
    url: "../asset/images/coccoa.png",
  },
  {
    id: "qsotux",
    name: "Dark Classic",
    price: 3.8,
    quantity: 4,
    description: "With 70% cocoa 50g",
    url: "../asset/images/DarkClassic.jpg",
  },
  {
    id: "rnueqo",
    name: "Endangered Species",
    price: 2.7,
    quantity: 17,
    description: "Strong and velvety with 88% cocoa 85g",
    url: "../asset/images/EndangeredSpecies.jpg",
  },
  {
    id: "cgreuu",
    name: "Galaxy",
    price: 3.6,
    quantity: 9,
    description: "Galaxy Nobe chocolate with Mango",
    url: "../asset/images/Galaxy.jpg",
  },
  {
    id: "cxzewq",
    name: "Godiva",
    price: 15.7,
    quantity: 47,
    description: "Milk chocolate with creamy hazelnut praline filling",
    url: "../asset/images/Godiva.jpg",
  },
  {
    id: "asddtre",
    name: "Copper HorseMan",
    price: 7.4,
    quantity: 18,
    description: "liqueur filling and milk fudge glazed with chocolate glaze",
    url: "../asset/images/LIQUEUR-copperHorseman.jpg",
  },
  {
    id: "ubbewyw",
    name: "Kenz",
    price: 7.8,
    quantity: 23,
    description: "chocolate ice cream with hazelnut",
    url: "../asset/images/HazelNut.png",
  },
  {
    id: "twvnt",
    name: "Queen of Spades",
    price: 7.5,
    quantity: 53,
    description: "liqueur filling and milk fudge glazed with chocolate glaze",
    url: "../asset/images/LIQUEUR-QueenOfSpades.jpg",
  },
  {
    id: "xbytwqn",
    name: "Natif",
    price: 14.5,
    quantity: 75,
    description: "Bread with Chocolate, sweetened with Stevia, vegan",
    url: "../asset/images/Natif.jpg",
  },
  {
    id: "cxzewq",
    name: "Serra do Conduru",
    price: 25.9,
    quantity: 52,
    description: "Milk chocolate with creamy hazelnut praline filling",
    url: "../asset/images/SerraDo.jpg",
  },
  {
    id: "weve65fg",
    name: "Tablete ao Leite",
    price: 16,
    quantity: 7,
    description:
      "Nugali`s 45% cocoa milk chocolate is formulated with a smooth blend of cocoas",
    url: "../asset/images/aoLeite.jpg",
  },
  {
    id: "seyveqf",
    name: "Raspberry & HazelNut",
    price: 2.4,
    quantity: 38,
    description:
      "This bar expertly combines  with pieces of raspberry and chopped hazelnuts",
    url: "../asset/images/Raspberry.jpg",
  },
  {
    id: "ngrawi",
    name: "Salted Caramel",
    price: 6.6,
    quantity: 24,
    description:
      "Made with the finest Trinitario cocoa beans for intense taste. 70% Cocoa",
    url: "../asset/images/SaltedCaramel.jpg",
  },
  {
    id: "evmqag",
    name: "Sea Salt",
    price: 5.9,
    quantity: 15,
    description: "Milk chocolate with creamy hazelnut praline filling",
    url: "../asset/images/SeaSalt.jpg",
  },
  {
    id: "vewmxrw",
    name: "Wicked",
    price: 16.2,
    quantity: 12,
    description: "Dark chocolate tablet with blueberries & almond cookies",
    url: "../asset/images/Vicked.jpg",
  },
];

function productInit() {
  // SaveDataToLocalStorage()
  mapProduct();
  request.totalItem();
}

function SaveDataToLocalStorage() {
  request.setItemLocalStorage(keyLocalStorageListSP, listData);
}

function mapProduct() {
  let getData = request.getItemLocalStorage(keyLocalStorageListSP);
  let listItem = document.querySelector(".listItem");
  if (listItem)
    listItem.innerHTML = getData
      .map((data) => {
        return `
                    <div class="item">
                        <img src="${data.url}" alt="">
                        <div class="item-content flex">
                            <h1 class="item-title">${data.name}</h1>
                            <p class="item-description">${data.description}</p>
                            <h4 class="item-quantity">Quantity: ${
                              data.quantity
                            }</h4>
                            <div class="item-price flex">Price: ${
                              data.price
                            } $</div>
                        </div>         
                        <!-- check quantity to show the right tag -->
                        ${
                          data.quantity > 0
                            ? `
                               <!-- button add to cart -->  
                                <div class="wrap-btn flex">
                                    <div 
                                            id="liveToastBtn"
                                        class="btn-add flex" 
                                        onclick="addSP(
                                         '${data.id}',
                                         '${data.name}',
                                          '${data.price}',
                                          '${data.description}',
                                         '${data.url}'
                                         )"
                                    >
                                        Add to cart
                                    </div>    
                                </div> 
                            `
                            : // <!-- sold out-->
                              `
                                <div class="sold-out flex">
                                    <div class="sold-out-circle flex">
                                        Sold Out
                                    </div>
                                </div>
                            `
                        }
                    </div>
                `;
      })
      .join("");
}

let cart = [];

function addSP(id, name, price, description, url) {
  let product = {
    id: id,
    name,
    price,
    soLuong: 1,
    description,
    url,
    total: price,
  };
  let check = false;
  let listData = request.getItemLocalStorage(keyLocalStorageListSP); //get list product form Localstorage
  let cart = request.getItemLocalStorage(keyCart);
  listData.find((item, index) => {
    if (id === item.id) {
      if (parseFloat(listData[index].quantity) > 0) {
        if (cart) {
          //If the item is already in the cart, the quantity will be increased by 1
          cart.find((item) => {
            if (item.id === id) {
              item.soLuong++;
              check = true;
            }
          });
          // If the item is not in the cart, the item will be added
          if (check === false) {
            cart.push(product);
          }
        } else {
          cart = [product]; // add item when cart is empty
        }
        request.setItemLocalStorage(keyCart, cart);
        request.totalItem(); //recalculate the total price of the product
        listData[index].quantity--;
        request.setItemLocalStorage(keyLocalStorageListSP, listData);
        mapProduct();
        alertMessage("This product has been added to cart", "success");
      } else {
        alertMessage("This product is run out of stock", "error");
      }
    }
  });
}

productInit();
