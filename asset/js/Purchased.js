(async () => {
  await mapOrders();
})();

async function mapOrders() {
  let orders = await request.getOrderApi();
  console.log(orders)
  document.querySelector(".table__body").innerHTML =
    orders.length <= 0
      ? `<p class="empty__data flex">No order found</p>`
      : orders
          ?.map((order, index) => {
            return `
            <tr class="body__row flex">
                <td class="body__cell code__cell flex">
                    <p class="order__code">
                    
                    ${order.id}
                    </p>
                    <div class="tooltip__code tooltip__src">
                        ${order.id}
                    </div>
                    <div class="btn__details${index} detail__container" >
                        <div class=" flex" onclick="rotateCaret('${index}')" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample${index}" aria-expanded="false" aria-controls="collapseExample">
                            Details
                            <div class="icon__container-${index} caret__rotation">    
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                </svg>
                            </div>
                        </div>
                    </p>
                </td>
                <td class="body__cell order-name">${order.customer.firstName} ${
              order.customer.lastName
            }</td>
                <td class="body__cell">${order.createByDate}</td>
                <td class="body__cell">${order.totalItem}</td>
                <td class="body__cell">${order.totalQuantity}</td>
                <td class="body__cell">${order.totalPrice} $</td>
                <td class="body__cell ">
                    <button 
                    type="button" 
                    class="btn__return"
                    onclick="openConfirmReturn('${order.id}')"
                    >
                        <img src="../asset/images/exchange.png" class="return__icon" alt="">
                    </button>
                </td>
            </tr>
            
            <!-- Details -->
            <div class="order__details" >
                <div class="collapse" id="collapseExample${index}">
                    <div class="list__container">
                    <!-- header -->
                        <div class="table__header flex">
                            <div class="header__cell row-img"></div>
                            <div class="header__cell ">Name</div>
                            <div class="header__cell ">Price</div>
                            <div class="header__cell ">Number</div>
                            <div class="header__cell ">Total</div>
                            <div class="header__cell btn__delete"></div>
                        </div>
                    <!-- body -->
                        <div class="row__body flex">
                            ${
                              order.orderDetail.length <= 0
                                ? `<p class="empty__data flex">No product in order</p>`
                                : order.orderDetail
                                    .map((item) => {
                                      return `
                                <div class="body__row flex">
                                    <div class="body__cell img__cell">
                                        <img src="${item.url}" alt="" class="img__body">
                                    </div>
                                    <div class="body__cell item-name">${item.name}</div>
                                    <div class="body__cell">${item.price} $ per one</div>
                                    <div class="body__cell">${item.soLuong}</div>
                                    <div class="body__cell">${item.total} $</div>
                                </div>
                            `;
                                    })
                                    .join("")
                            }
                        </div>
                    </div>
                </div>
            </div>
            <!-- divider -->
            <div class="row__divider"></div>  
        `;
          })
          .join("");
}

function rotateCaret(index) {
  if (!document.querySelector(".caret-up")) {
    document
      .querySelector(".icon__container-" + index)
      .classList.add("caret-up");
  } else {
    document
      .querySelector(".icon__container-" + index)
      .classList.remove("caret-up");
  }
}

//Dialog actions
const openConfirmReturn = (orderID) => {
  document.querySelector(".return__dialog").style.display = "flex";
  request.setItemLocalStorage(keyOrderReturn, orderID);
};
const closeConfirmReturn = () => {
  console.log("a");
  document.querySelector(".return__dialog").style.display = "none";
  request.removeItemLocalStorage(keyOrderReturn);
};

async function confirmReturn() {
  console.log("a");
  let orderReturnID = request.getItemLocalStorage(keyOrderReturn);
  let orders = await request.getOrderApi();
  let listProduct = request.getItemLocalStorage(keyLocalStorageListSP);
  orders.find(async (order) => {
    if (order.id === orderReturnID && order.orderDetail.length > 0) {
      //  choose right product to return quantity to stock
      order.orderDetail.forEach((item) => {
        listProduct.find((product) => {
          if (item.id === product.id) product.quantity += item.soLuong;
        });
      });
      request.setItemLocalStorage(keyLocalStorageListSP, listProduct);
    }
  });
  await request.deleteOrderApi(orderReturnID).then((res) => {
    closeConfirmReturn();
    mapOrders();
    alertMessage("The order has been returned");
  });
}
