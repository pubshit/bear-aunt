function Validate(inputElement, errorElement, rule) {
    let errorMessage = rule.test(inputElement.value)
    errorElement.innerHTML = ''
    inputElement.classList.remove('invalid')
    if (errorMessage) {
        errorElement.innerHTML = errorMessage
        inputElement.classList.add('invalid')
    }
}

function validator(options) {
    //get element of form need validate
    let formElement = document.querySelector(options.form)

    if (formElement) {
        options.rules.forEach((rule) => {
            let inputElement = formElement.querySelector(rule.selector)
            let errorElement = inputElement.parentElement.parentElement.querySelector(options.errorSelector)

            if (inputElement) {
                //handle onblur
                inputElement.onblur = function () {
                    Validate(inputElement, errorElement, rule)
                }

                //handle onchange
                inputElement.oninput = () => {
                    errorElement.innerHTML = ''
                    inputElement.classList.remove('invalid')
                }
            }
        })
    }
}


////Define rules

validator.isRequired = function (selector) {
    let fieldName = ''
    if (selector === '#input-first-name') fieldName = 'First name is required'
    else if (selector === '#input-last-name') fieldName = 'Last name is required'
    else if (selector === '#input-phone') fieldName = 'Phone number is required'
    else if (selector === '#input-address') fieldName = 'Please fill specific address'
    else if (selector === '#input-note') fieldName = 'Please leave some note'
    return {
        selector: selector,
        test: function (value) {
            return value.trim() ? undefined : fieldName
        }
    }
}


validator.isEmail = function (selector) {
    return {
        selector: selector,
        test: function (value) {

            return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value) ? undefined : 'Invalid email'
        }
    }
}


validator.isRequiredSelect = function (selector) {
    return {
        selector: selector,
        test: function (value) {
            let checkValue
            if (value.trim() === 'Provinces...') checkValue = 'province'
            else if (value.trim() === 'Districts...') checkValue = 'district'
            else if (value.trim() === 'Communes...') checkValue = 'commune'

            return checkValue ? ('Please choose the ' + checkValue + '!') : undefined
        }
    }
}


function refreshDialog() {
    //refresh input message
    const dialog = document.querySelector('.dialog')
    const allInput = dialog.querySelectorAll('.input__refresh')
    for (let i = 0; i < allInput.length; i++) {
        allInput[i].value = ''
        allInput[i].classList.remove('invalid')
        console.log(allInput[i].parentElement.parentElement);
        allInput[i]
            .parentElement
            .parentElement
            .querySelector('.form__message')
            .innerHTML = ''
    }
    //refresh select messaage
    const allSelector = dialog.querySelectorAll('.selector')
    for (let i = 0; i < allSelector.length; i++) {
        allSelector[i].classList.remove('invalid')
        allSelector[i].parentElement.parentElement
            .querySelector('.form__message').innerHTML = ''
    }
}


function checkSubmit(customer) {
    return !(customer.firstName === '' ||
        customer.lastName === '' ||
        customer.email === '' ||
        !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(customer.email)) ||
        customer.phone === '' ||
        customer.phone.length === 10 ||
        customer.address === '' ||
        customer.note === '' ||
        customer.province === {} ||
        customer.district === {} ||
        customer.commune === {});
}


//---- Toast ----//



function alertMessage(message, type) {

    const toastLiveExample = document.getElementById('liveToast')
    const toastHeaderTitle = document.querySelector('.me-auto')
    const toastBody = document.querySelector('.toast-body')

    toastHeaderTitle?.parentElement.classList.remove('error')
    toastHeaderTitle?.parentElement.classList.remove('success')
    toastHeaderTitle?.parentElement.classList.remove('warning')

    if (type === 'success') {
        toastHeaderTitle.parentElement.classList.add(type)
        toastHeaderTitle.innerHTML = 'Success'
    } else if (type === 'warning') {
        toastHeaderTitle.parentElement.classList.add(type)
        toastHeaderTitle.innerHTML = 'Warning'
    } else if (type === 'error') {
        toastHeaderTitle.parentElement.classList.add(type)
        toastHeaderTitle.innerHTML = 'Error'
    }

    // toastHeaderTitle.parentElement.classList.add(type)
    // toastHeaderTitle.innerHTML = 'Success'

    if (toastBody) toastBody.innerHTML = message
    const toast = new bootstrap.Toast(toastLiveExample)
    // console.log(toast)
    toast.show()
    // setTimeout(function () {
    //     toast.hide()
    // }, 2500)
}