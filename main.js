const keyLocalStorageListSP = "DANHSACHSP";
const keyLocalStorageItemCart = "DANHSACHITEMCART";
const keyCart = "ItemCartList";
const keyItemRemove = "ProductRemoveID";
const keyOrderReturn = "OrderReturnID";

//---- Scroll ----//

function moveDown() {
  const wrapper = document.querySelector(".wrapper");
  wrapper.scrollTo({
    top: 500,
    behavior: "smooth",
  });
}

function moveUp() {
  const wrapper = document.querySelector(".wrapper");
  wrapper.scrollTo({
    top: 0,
    behavior: "smooth",
  });
}

//while Scrolling
function Scrolling() {
  // console.log(document.querySelector('.wrapper').scrollTop)
  const wrapper = document.querySelector(".wrapper");
  const downArrow = document.querySelector(".down-arrow");
  const blurTop = document.querySelector(".blur-top");
  if (wrapper.scrollTop >= 500) {
    downArrow.style.display = "none";
    blurTop.style.display = "none";
  } else {
    if (blurTop && downArrow) {
      blurTop.style.display = "block";
      downArrow.style.display = "flex";
    }
  }
}

let request = (() => {
  ////LocalStorage request
  const setItemLocalStorage = (key, data) => {
    return window.localStorage.setItem(key, JSON.stringify(data));
  };

  const getItemLocalStorage = (key) => {
    return JSON.parse(window.localStorage.getItem(key));
  };

  const removeItemLocalStorage = (key) => {
    return window.localStorage.removeItem(key);
  };

  ////APIs
  //Order
  const getOrderApi = async () => {
    const response = await fetch("http://localhost:3000/orders");
    return response.json(); // parses JSON response into native JavaScript objects
  };

  const postOrderApi = async (data) => {
    const response = await fetch("http://localhost:3000/orders", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
  };

  const putOrderApi = async (id, data) => {
    const response = await fetch(`http://localhost:3000/orders/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    console.log(response);
    return response.json(); // parses JSON response into native JavaScript objects
  };

  const deleteOrderApi = async (id) => {
    const response = await fetch(`http://localhost:3000/orders/${id}`, {
      method: "DELETE",
    });

    return response.json(); // parses JSON response into native JavaScript objects
  };

  const getAllOrderId = async () => {
    let orderData = await request.getOrderApi();
    let ordersId = [];
    orderData.map((order) => {
      ordersId.push(order.id);
    });
    return ordersId;
  };

  //Province, District, Ward
  const getProvincesApi = async () => {
    return (await fetch("https://provinces.open-api.vn/api/p/")).json();
  };

  const getDistrictsApi = async () => {
    return (await fetch("https://provinces.open-api.vn/api/d/")).json();
  };

  const getCommunesApi = async () => {
    return (await fetch("https://provinces.open-api.vn/api/w/")).json();
  };

  ////Calculate the price, bill and products number

  // prototype
  Array.prototype.totalPrice = function () {
    for (let i = 0; i < this.length; i++) {
      let total = parseFloat(this[i].soLuong) * parseFloat(this[i].price);
      this[i].total = Math.round(total * 100) / 100;
    }
  };

  Array.prototype.totalCartProducts = function () {
    let product = 0;
    for (let i = 0; i < this.length; i++) {
      product += parseFloat(this[i].soLuong);
    }
    return product;
  };

  Array.prototype.bill = function () {
    let bill = 0;
    for (let i = 0; i < this.length; i++) {
      bill += parseFloat(this[i].total);
    }
    return Math.round(bill * 100) / 100;
  };

  //calculation methods
  const totalItem = () => {
    let cart = request.getItemLocalStorage(keyCart);
    if (cart) {
      cart?.totalPrice();
      request.setItemLocalStorage(keyCart, cart);

      //show cart quantity in other pages
      let cartNumber = document.querySelector(".product-number");
      if (cartNumber) {
        cartNumber.style.display = "flex";
        cartNumber.innerHTML = cart.totalCartProducts().toString();
      }

      //reset bill and data notice
      let totalProductElement = document.querySelector(".total__product");
      let totalPriceElement = document.querySelector(".total__Price");
      if (totalProductElement && totalPriceElement) {
        totalProductElement.innerHTML = "Total Number of Product: 0";
        totalPriceElement.innerHTML = "Total of Price: 0 $";
        document.querySelector(".empty__data").innerHTML =
          "No product have been added";
        if (cart.length > 0) {
          totalProductElement.innerHTML =
            "Total Number of Product: " + cart?.totalCartProducts();
          totalPriceElement.innerHTML =
            "Total of Price: " + cart?.bill() + " $";
        }
      }
    }
  };

  function totalCartProducts(cart) {
    return cart.reduce(
      (accumulator, currentValue) => accumulator + currentValue.soLuong,
      0
    );
  }

  function bill(cart) {
    return cart.reduce(
      (accumulator, currentValue) => accumulator + currentValue.total,
      0
    );
  }

  return {
    //LocalStorage
    setItemLocalStorage,
    getItemLocalStorage,
    removeItemLocalStorage,

    //order
    getOrderApi,
    postOrderApi,
    putOrderApi,
    deleteOrderApi,
    getAllOrderId,

    //Province, District, Ward
    getProvincesApi,
    getDistrictsApi,
    getCommunesApi,

    //calculation
    totalItem,
    bill,
    totalCartProducts,
  };
})();
